# README #

Welcome to an unofficial Fork of phronks "Insurgency Takistan" for ArmA 3.

Feel free to Fork yourself,adjust the code or just comment on things.

Aim of this Project is to improve and personalize the fantastic work that was done by phr0nk and come closer to an epic Insurgency type mission that the famous 
MilGo Group had on their ArmA2 OA Servers.

### What is this repository for? ###

* Keep track of changes
* Never worry about lost fixes
* Finish a project once.Seriously,we sit on that since almost 2 years in total

### How do I get set up? ###

* The mission depends on the following Mods:
@CBA_A3 @CUP_Terrain @CUP_Weapons @CAF_AG1.5 @HLC_Mods @SMAV2.3 @NATO_Rus_Weapons @TalibanFighters

So far,we are using ELITENESS to pack the PBO and it seems to work well,even with 3DEN.

NOTE: IF you host the mission on a dedicated Server,clients will recieve an error regarding the Doors on the Mosque at the OPFOR Base and get kicked.
      Rejoining will let them play without issues.This is an error that gets created trough CUP_Terrains and is hopefuly fixed in the next version of the Mod.

### Contribution guidelines ###

* If you can,improve the wonky code
* Add things that are missing (make sure to keep it working on a dedicated Server setup,locality is a big issue)
* Leave comments
* Give credit to content/scripts taken from anywhere

### Who do I talk to? ###

* Repo owner or admin