v.0.91

- Ported Mission to 3DEN
- Ported Mission to CUP Terrain instead of AiA (dropped legacy support for AiA)
- added Close Air support to Recon units
- removed vanilla radiochatter/status call of units (unda faia!!!)
- better CUP/HLC AK PACK integration into insurgent groups
- changed OPFOR Teleport object from Flags to Wells (IS flags still used to mark the spots)
- weaponcaches should have the correct object now (crates with camo netting)
- added custom troop/squad script that lets other people join different squadleaders (lets you have 2 medics for example)
- updated SMA to 2.3 
- updated CUP WEAPONS to 1.5
- tweaked enemy spawn,it is not boring when you have overwatch now [town north of blufor base toned down to limit firing across the hills while equipping]
- tweaked OPFOR spawn to be invisible until placed properly by server,no more ghosts until population kicks in
- OPFOR can see pre-spawned Intel AND Weaponcaches now (removing the Intel raises the chance for the caches to remain hidden longer AND caches can be made a bit more secure )
- AI suicide bombers carry visible explosive belts (timer regarding running to enemy and blowing up needs to be lowered)
- blufor medics can deploy sandbags now
- added IED crate to OPFOR base were they can equip a suicide west,recieve an addaction to "detonate vest" and yell alahu akbar with a boom. Explosives are visible on playermodel
- reworked OPFOR base,it is a semi big complex with an old mosque now instead of some selfmade shack and has basic instructions in form of info-panels
- added sound to civilian assassins to warn BLUFOR about armed civilians
- OPFOR Player Squad uses proper models and equipment now
- fixed evac helo sperging out [should drop off squad and turn off engines until called again] // scratch that,still spergs out and does whatever it wants <- LOCALITY ISSUE

in the works: 
civilian killcounter with bad things happening to civkillers
