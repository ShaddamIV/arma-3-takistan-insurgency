while {alive _this} do{

//Settings 
_weaponAmount = 15;
_magAmount = 25;
_itemAmount = 20;
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;


/*_this additemcargoGlobal ["optic_MRCO", _itemAmount];
_this additemcargoGlobal ["optic_Holosight_smg", _itemAmount];
_this additemcargoGlobal ["optic_Holosight", _itemAmount];
_this additemcargoGlobal ["optic_Hamr", _itemAmount];
_this additemcargoGlobal ["optic_Arco", _itemAmount];
_this additemcargoGlobal ["optic_Aco_smg", _itemAmount];
_this additemcargoGlobal ["optic_ACO_grn_smg", _itemAmount];
_this additemcargoGlobal ["optic_ACO_grn", _itemAmount];
_this additemcargoGlobal ["optic_Aco", _itemAmount];
_this additemcargoGlobal ["optic_SOS", _itemAmount];
_this additemcargoGlobal ["optic_DMS", _itemAmount];
_this additemcargoGlobal ["optic_LRPS", _itemAmount];
_this additemcargoGlobal ["optic_NVS", _itemAmount];
_this additemcargoGlobal ["optic_Yorris", _itemAmount];
_this additemcargoGlobal ["muzzle_snds_M", _itemAmount];
_this additemcargoGlobal ["muzzle_snds_L", _itemAmount];
_this additemcargoGlobal ["muzzle_snds_H_MG", _itemAmount];
_this additemcargoGlobal ["muzzle_snds_H", _itemAmount];
_this additemcargoGlobal ["muzzle_snds_B", _itemAmount];
_this additemcargoGlobal ["muzzle_snds_acp", _itemAmount];
_this additemcargoGlobal ["acc_pointer_IR", _itemAmount];
_this additemcargoGlobal ["acc_flashlight", _itemAmount];*/
_this additemcargoGlobal ["Medikit", 1];
//_this additemcargoGlobal ["ToolKit", _itemAmount];
//_this additemcargoGlobal ["ItemGPS", _itemAmount];
//_this additemcargoGlobal ["ItemRadio", _itemAmount];
_this additemcargoGlobal ["FirstAidKit", 20];
_this additemcargoGlobal ["NVGoggles", 1];
/*_this additemcargoGlobal ["B_UavTerminal", _itemAmount];
_this additemcargoGlobal ["H_Shemag_tan", _itemAmount];
_this additemcargoGlobal ["H_Beret_blk", _itemAmount];
_this additemcargoGlobal ["H_Bandanna_khk", _itemAmount];
_this additemcargoGlobal ["H_Booniehat_mcamo", _itemAmount];
_this additemcargoGlobal ["H_HelmetB_camo", _itemAmount];
_this additemcargoGlobal ["U_B_GhillieSuit", 2];*/

//_this addweaponcargoGlobal ["Laserdesignator", _itemAmount];
_this addweaponcargoGlobal ["Binocular", 1];
_this addweaponcargoGlobal ["Rangefinder", 1];
/*_this addweaponcargoGlobal ["srifle_LRR_F", _weaponAmount];
_this addweaponcargoGlobal ["srifle_GM6_F", _weaponAmount];
_this addweaponcargoGlobal ["srifle_EBR_F", _weaponAmount];
_this addweaponcargoGlobal ["LMG_Mk200_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_SDAR_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_MXM_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_MXC_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_MX_SW_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_MX_GL_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_MX_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_Mk20C_plain_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_Mk20_plain_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_Mk20_GL_plain_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_Mk20_GL_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_Mk20_F", _weaponAmount];
_this addweaponcargoGlobal ["srifle_DMR_01_F", _weaponAmount];
_this addweaponcargoGlobal ["launch_B_Titan_short_F", _weaponAmount];
_this addweaponcargoGlobal ["launch_B_Titan_F", _weaponAmount];
_this addweaponcargoGlobal ["launch_NLAW_F", _weaponAmount];
_this addweaponcargoGlobal ["launch_RPG32_F", _weaponAmount];
_this addweaponcargoGlobal ["hgun_Rook40_F", _weaponAmount];
_this addweaponcargoGlobal ["hgun_ACPC2_F", _weaponAmount];*/
_this addweaponcargoGlobal ["MineDetector", 1];

_this addmagazinecargoGlobal ["30Rnd_556x45_Stanag", _magAmount];
/*_this addmagazinecargoGlobal ["30Rnd_556x45_Stanag_Tracer_Red", _magAmount];
_this addmagazinecargoGlobal ["30Rnd_556x45_Stanag_Tracer_Green", _magAmount];
_this addmagazinecargoGlobal ["30Rnd_556x45_Stanag_Tracer_Yellow", _magAmount];*/
_this addmagazinecargoGlobal ["30Rnd_65x39_caseless_mag", _magAmount];
_this addmagazinecargoGlobal ["100Rnd_65x39_caseless_mag", _magAmount];
_this addmagazinecargoGlobal ["200Rnd_65x39_cased_Box", 5];
_this addmagazinecargoGlobal ["200Rnd_65x39_cased_Box_Tracer", 5];
_this addmagazinecargoGlobal ["20Rnd_762x51_Mag", _magAmount];
_this addmagazinecargoGlobal ["10Rnd_762x51_Mag", _magAmount];
_this addmagazinecargoGlobal ["5Rnd_127x108_Mag", _magAmount];
_this addmagazinecargoGlobal ["7Rnd_408_Mag", _magAmount];
_this addmagazinecargoGlobal ["9Rnd_45ACP_Mag", _magAmount];
_this addmagazinecargoGlobal ["20Rnd_556x45_UW_mag", _magAmount];
_this addmagazinecargoGlobal ["Titan_AT", 10];
_this addmagazinecargoGlobal ["Titan_AA", 5];
_this addmagazinecargoGlobal ["NLAW_F", 10];
_this addmagazinecargoGlobal ["RPG32_HE_F", 10];
_this addmagazinecargoGlobal ["RPG32_F", 10];
_this addmagazinecargoGlobal ["1Rnd_HE_Grenade_shell", 15];
_this addmagazinecargoGlobal ["1Rnd_Smoke_Grenade_shell", 10];
/*_this addmagazinecargoGlobal ["1Rnd_SmokeBlue_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_SmokeGreen_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_SmokeOrange_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_SmokePurple_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_SmokeRed_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_SmokeYellow_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["UGL_FlareCIR_F", _magAmount];
_this addmagazinecargoGlobal ["UGL_FlareGreen_F", _magAmount];
_this addmagazinecargoGlobal ["UGL_FlareRed_F", _magAmount];*/
_this addmagazinecargoGlobal ["UGL_FlareYellow_F", 15];
_this addmagazinecargoGlobal ["DemoCharge_Remote_Mag", 5];
_this addmagazinecargoGlobal ["SatchelCharge_Remote_Mag", 5];
_this addmagazinecargoGlobal ["ClaymoreDirectionalMine_Remote_Mag", 2];
_this addmagazinecargoGlobal ["ATMine_Range_Mag", 2];
//_this addmagazinecargoGlobal ["APERSMine_Range_Mag", _magAmount];
//_this addmagazinecargoGlobal ["APERSBoundingMine_Range_Mag", _magAmount];
//_this addmagazinecargoGlobal ["SLAMDirectionalMine_Wire_Mag", _magAmount];
//_this addmagazinecargoGlobal ["APERSTripMine_Wire_Mag", _magAmount];
_this addmagazinecargoGlobal ["HandGrenade", 15];
//_this addmagazinecargoGlobal ["MiniGrenade", _magAmount];
_this addmagazinecargoGlobal ["SmokeShell", 15];
/*_this addmagazinecargoGlobal ["SmokeShellYellow", _magAmount];
_this addmagazinecargoGlobal ["SmokeShellGreen", _magAmount];
_this addmagazinecargoGlobal ["SmokeShellRed", _magAmount];
_this addmagazinecargoGlobal ["SmokeShellPurple", _magAmount];
_this addmagazinecargoGlobal ["SmokeShellOrange", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_green", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_red", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_yellow", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_blue", _magAmount];*/

sleep _sleepAmount;

};