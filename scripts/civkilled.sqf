_killed = _this select 0;
_killer = _this select 1;

if (_killer == vehicle _killer && isPlayer _killer) then { //not a vehicle and a player
	player globalChat format ["A civilian was killed by %1!", name _killer];
	player addscore -5;

	if (_killer == vehicle player) then { // code for killer's machine
		_civskilled = (player getVariable ["civskilled", 0]) + 1;
		player setVariable ["civskilled", _civskilled];
	
		switch (_civskilled) do {
			case 1: {
				player globalChat format ["WARNING: You will be auto-punished for killing civilians"];
				sleep 2;
				player globalChat format ["WARNING: You will be auto-punished for killing civilians"];
			};
			case 2: {
				disableUserInput true;
				player globalChat format ["You have been disabled for 30 seconds for killing a civilian"];
				sleep 30;
				disableUserInput false;
			};
			case 3: {
				disableUserInput true;
				player globalChat format ["You have been disabled for 1 minute for killing a civilian"];
				sleep 60;
				disableUserInput false;
			};
			case 4: {
				disableUserInput true;
				player globalChat format ["You have been disabled for 2 minutes for killing a civilian"];
				sleep 120;
				disableUserInput false;
			};
			case 5: {
				player globalChat format ["You are being killed for killing a civilian"];
				sleep 5;
				player setDamage 1;
			};
			case 6: {
				player globalChat format ["WARNING: You are being kicked for Rules of Engagement violations"];
				sleep 5;
				endMission "LOSER";
			};
		};
	};
};




