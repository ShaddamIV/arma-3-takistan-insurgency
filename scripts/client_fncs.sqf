JIG_placeSandbag_fnc = {
	// Player action place sandbag barrier. by Jigsor
	private ["_player","_dist","_zfactor","_zvector"];
	_player = _this select 1;
	if(vehicle _player != player) exitWith {hintSilent "You need to be on Foot to deploy"};

	_dist = 2;
	_zfactor = 1;
	_zvector = ((_player weaponDirection (primaryWeapon _player)) select 2) * 3;

	if (not (isNull MedicSandBag)) then {deleteVehicle MedicSandBag;};
	MedicSandBag = createVehicle ["Land_BagFence_Round_F",[(getposATL _player select 0) + (sin(getdir _player) * _dist), (getposATL _player select 1) + (cos(getdir _player) * _dist)], [], 0, "CAN_COLLIDE"];

	MedicSandBag setposATL [(getposATL _player select 0) + (sin(getdir _player) * _dist), (getposATL _player select 1) + (cos(getdir _player) * _dist), (getposATL _player select 2) + _zvector + _zfactor];
	MedicSandBag setDir getDir (_this select 1) - 180;
	if ((getPosATL MedicSandBag select 2) > (getPosATL _player select 2)) then {MedicSandBag setPos [(getPosATL MedicSandBag select 0), (getPosATL MedicSandBag select 1), (getPosATL _player select 2)];};

	(_this select 1) removeAction (_this select 2);
	_id = MedicSandBag addAction ["Remove Sandbag", {call JIG_removeSandbag_fnc}];
};
JIG_removeSandbag_fnc = {
	// Player action remove sandbag barrier. by Jigsor
	deleteVehicle (_this select 0);
	_id = (_this select 1) addAction ["Remove Sandbag", {call JIG_placeSandbag_fnc}, 0, -9, false];
};

JIG_p_actions_resp = {
	// Add player actions. by Jigsor
	waitUntil {sleep 1; alive player};
	private "_playertype";
	_playertype = typeOf (vehicle player);
	// Medic
	if (_playertype in INS_W_PlayerMedic) then	{MedicSandBag = ObjNull; _id = player addAction[ "Place Sandbag",{call JIG_placeSandbag_fnc}, 0, -9, false];};
	
};