while {alive _this} do{

//Settings 
_weaponAmount = 5;
_magAmount = 100;
_itemAmount = 15;
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;




//_this addweaponcargoGlobal ["Laserdesignator", _itemAmount];

_this additemcargoGlobal ["optic_mas_zeiss", _itemAmount];
_this additemcargoGlobal ["optic_mas_zeiss_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_zeiss_eo", _itemAmount];
_this additemcargoGlobal ["optic_mas_zeiss_eo_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_eo", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_eo_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_rd", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_rd_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_aim_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_DMS", _itemAmount];
_this additemcargoGlobal ["optic_mas_DMS_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_Holosight_blk", _itemAmount];
_this additemcargoGlobal ["optic_mas_Holosight_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_Arco_blk", _itemAmount];
_this additemcargoGlobal ["optic_mas_Arco_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_Hamr_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_Aco_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_ACO_grn_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_MRCO_camo", _itemAmount];


_this addweaponcargoGlobal ["srifle_mas_m24_d", _weaponAmount];
_this addweaponcargoGlobal ["srifle_mas_ksvk", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_Mk200_F", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_M249_F", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_Mk48_F", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_M240_F", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_mg3_F", _weaponAmount];
_this addweaponcargoGlobal ["mas_launch_RPG18_F", _weaponAmount];
_this addweaponcargoGlobal ["mas_launch_M136_F", _weaponAmount];
_this addweaponcargoGlobal ["mas_launch_Stinger_F", _weaponAmount];


_this addmagazinecargoGlobal ["5Rnd_mas_762x51_T_Stanag", _magAmount];
_this addmagazinecargoGlobal ["5Rnd_mas_127x108_mag", _magAmount];
_this addmagazinecargoGlobal ["200Rnd_mas_556x45_T_Stanag", _magAmount];
_this addmagazinecargoGlobal ["150Rnd_762x51_Box_Tracer", _magAmount];
_this addmagazinecargoGlobal ["mas_PG18", _magAmount];
_this addmagazinecargoGlobal ["mas_M136", _magAmount];
_this addmagazinecargoGlobal ["mas_Stinger", _magAmount];
_this addmagazinecargoGlobal ["HandGrenade", _magAmount];
_this addmagazinecargoGlobal ["MiniGrenade", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_Smoke_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_green", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_red", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_yellow", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_blue", _magAmount];

sleep _sleepAmount;

};