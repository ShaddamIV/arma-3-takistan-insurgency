while {alive _this} do{

//Settings 
_weaponAmount = 5;
_magAmount = 100;
_itemAmount = 15;
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;


_this additemcargoGlobal ["SMA_AIMPOINT", _itemAmount];
_this additemcargoGlobal ["SMA_AIMPOINT_GLARE", _itemAmount];
_this additemcargoGlobal ["SMA_ELCAN_SPECTER", _itemAmount];
_this additemcargoGlobal ["SMA_ELCAN_SPECTER_TAN", _itemAmount];


//_this addweaponcargoGlobal ["Laserdesignator", _itemAmount];
_this addweaponcargoGlobal ["SMA_HK416afg", _weaponAmount];
_this addweaponcargoGlobal ["SMA_HK416vfg", _weaponAmount];
_this addweaponcargoGlobal ["SMA_HK416GL", _weaponAmount];
_this addweaponcargoGlobal ["SMA_HK417afg", _weaponAmount];
_this addweaponcargoGlobal ["SMA_HK417vfg", _weaponAmount];
_this addweaponcargoGlobal ["SMA_HK417_16in_afg", _weaponAmount];
_this addweaponcargoGlobal ["SMA_HK417_tanafg", _weaponAmount];
_this addweaponcargoGlobal ["SMA_HK417_tanvfg", _weaponAmount];
_this addweaponcargoGlobal ["SMA_HK417_16in_afg_tan", _weaponAmount];
_this addweaponcargoGlobal ["SMA_SAR21_F", _weaponAmount];
_this addweaponcargoGlobal ["SMA_SAR21_AFG_F", _weaponAmount];
_this addweaponcargoGlobal ["SMA_SKS_F", _weaponAmount];
_this addweaponcargoGlobal ["SMA_STG_E4_F", _weaponAmount];
_this addweaponcargoGlobal ["SMA_STG_E4_BLACK_F", _weaponAmount];
_this addweaponcargoGlobal ["SMA_AUG_A3_F", _weaponAmount];
_this addweaponcargoGlobal ["SMA_AUG_A3_MCAM_F", _weaponAmount];
_this addweaponcargoGlobal ["SMA_AUG_A3_KRYPT_F", _weaponAmount];
_this addweaponcargoGlobal ["SMA_MK16", _weaponAmount];
_this addweaponcargoGlobal ["SMA_Mk17", _weaponAmount];
_this addweaponcargoGlobal ["SMA_Mk16_EGLM", _weaponAmount];
_this addweaponcargoGlobal ["SMA_Mk17_EGLM", _weaponAmount];
_this addweaponcargoGlobal ["SMA_MK16_EGLM_black", _weaponAmount];
_this addweaponcargoGlobal ["SMA_MK17_EGLM_black", _weaponAmount];

_this addmagazinecargoGlobal ["SMA_30Rnd_762x39_SKS", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_762x39_SKS_Red", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_M855A1", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_M855A1_Tracer", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_M855A1_IR", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_Mk318", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_Mk318_Tracer", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_Mk318_IR", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_Mk262", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_Mk262_Tracer", _magAmount];
_this addmagazinecargoGlobal ["SMA_30Rnd_556x45_Mk262_IR", _magAmount];
//_this addmagazinecargoGlobal ["SMA_20Rnd_762x51_Tracer_Mag", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_Smoke_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["HandGrenade", _magAmount];
_this addmagazinecargoGlobal ["MiniGrenade", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_green", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_red", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_yellow", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_blue", _magAmount];

sleep _sleepAmount;

};