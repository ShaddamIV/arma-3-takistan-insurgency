//this addAction ["Teleport into Combat","ms\opfor_teleport.sqf"];


/*
switch (worldName) do
{
	case "Takistan":{
	_NEARENEMY = 500;
	};
	case "Zargabad":{
	_NEARENEMY = 400;
	};
	case "Chernarus":{
	_NEARENEMY = 500;
	};
	case "esbekistan":{
	_NEARENEMY = 500;
	};
};
*/
		
TitleText[format['CLICK ON THE MAP TO TELEPORT'],'PLAIN DOWN'];
openMap [true, false];

onMapSingleClick "

_pos set [2,0];
_enemyunits = {side _x == west} count ((_pos) nearEntities [['Man','Car','APC','Tank','Helicopter','Plane','Ship','Support'],250]);

if (_enemyunits > 0) then {
		TitleText[format['CANNOT TELEPORT HERE, BLUFOR UNITS WITHIN 250 METERS'],'PLAIN DOWN'];
} else {
		player setPos _pos; onMapSingleClick ''; true;
		TitleText[format['YOU HAVE BEEN TELEPORTED SUCCESSFULLY'],'PLAIN DOWN'];
		openMap [false, false];
}";